import {bootstrap, provide} from 'angular2/angular2';
import {LightShowApp} from './app/light-show';
import {HTTP_PROVIDERS} from 'angular2/http';
import {GlobalSettingsService} from './app/services/global-settings-service/global-settings-service'
import {MusicService} from './app/services/music-service/music-service'
import {ROUTER_PROVIDERS, LocationStrategy, HashLocationStrategy} from 'angular2/router';

bootstrap(LightShowApp,[HTTP_PROVIDERS,ROUTER_PROVIDERS,GlobalSettingsService,MusicService,provide(LocationStrategy, {useClass: HashLocationStrategy})]);
