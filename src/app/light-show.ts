import {Component, bootstrap,} from 'angular2/angular2';
import {MusicLibraryComponent} from './components/music-library-component/music-library-component'
import {MusicQueueComponent} from './components/music-queue-component/music-queue-component'
import {NowPlayingComponent} from './components/now-playing-component/now-playing-component'
import {MusicHistoryComponent} from './components/music-history-component/music-history-component'
import {MusicService} from './services/music-service/music-service';
import {ROUTER_DIRECTIVES, RouteConfig, Route, Location} from 'angular2/router';

@Component({
  selector: 'light-show-app',
  providers: [MusicService],
  templateUrl: 'app/light-show.html',
  directives: [ROUTER_DIRECTIVES, MusicLibraryComponent, MusicQueueComponent, NowPlayingComponent, MusicHistoryComponent],
  pipes: []
})
@RouteConfig([
    new Route({path: '/:song', component: MusicQueueComponent, name: 'QueueAddedSong'}),
    new Route({path: '/', component: MusicQueueComponent, name: 'Queue'}),
    new Route({path: '/Library', component: MusicLibraryComponent, name: 'Library'}),
    new Route({path: '/History', component: MusicHistoryComponent, name: 'History'}),
])
export class LightShowApp {
  location: Location
  constructor(musicService: MusicService, location: Location) {
    this.location = location;
  }
  getLinkStyle(path) {
    return this.location.path() === path;
  }
}
