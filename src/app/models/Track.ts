export class Track {
	trackId: number;
	title: string;
	artist: string;
	minutes: number;
	seconds: number;
	totalSeconds: number;
	secondsLeft: number;
	secondsIn: number;
	playedDate: string;
	endDate: string;
	playedDateDisplay: Date;
	endDateDisplay: Date;
	millisLeft: number;
	finalTime: Date;
	timeIn: Date;
	percentagePlayed: number;
	//length: Date;
	constructor()
	constructor(obj?: Object);
	constructor(obj?: any){
		obj = obj || {};
		this.trackId = obj.trackId || 0;
		this.title = obj.title || '';
		this.artist = obj.artist || '';
		this.minutes = obj.minutes || 0;
		this.seconds = obj.seconds || 0;
		this.playedDate =  obj.playedDate;
		this.endDate =  obj.endDate;
		this.millisLeft = obj.millisLeft || 0;
		this.finalTime = new Date(new Date().getTime()+this.millisLeft);
		this.totalSeconds = this.minutes*60+this.seconds;
		this.secondsLeft = 0;
		this.secondsIn = 0;
		this.timeIn = new Date();
		this.percentagePlayed = 0;
		//this.playedDateDisplay = !!obj.playedDate ? new Date(obj.playedDate) : new Date();
		//this.endDateDisplay = !!obj.endDate ? new Date(obj.endDate) : new Date();
		
//		this.length = new Date(new Date(0).getTime()+this.minutes*60*1000+this.seconds*1000+6*60*60*1000);
	}
	getTimeLeft():Date{
		var timeLeft = new Date(this.finalTime.getTime()-new Date().getTime());
        if(timeLeft > new Date(0)){
			this.secondsLeft = timeLeft.getTime()/1000;
			this.secondsIn = this.totalSeconds - this.secondsLeft;
			this.percentagePlayed = this.secondsIn/this.totalSeconds*100;
			//this.timeIn = new Date(new Date().getTime() - this.finalTime.getTime() - timeLeft.getTime())
			this.timeIn = new Date(this.secondsIn*1000);
			return timeLeft;
		}
		this.timeIn = new Date(0);
		this.secondsIn = this.totalSeconds;
		this.secondsLeft = 0;
		this.percentagePlayed = 100;
		return null;
	}
}