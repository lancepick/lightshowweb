import {Component} from 'angular2/angular2';
import {Track} from '../../models/Track';
import {MusicService} from '../../services/music-service/music-service';

@Component({
  selector: 'now-playing-component',
  templateUrl: 'app/components/now-playing-component/now-playing-component.html',
  styleUrls: ['app/components/now-playing-component/now-playing-component.css'],
  providers: [],
  directives: [],
  pipes: []
})
export class NowPlayingComponent { 
  track: Track;
  timeLeft: Date;
  interval: any;
  percentageIn: number;
  timeIn: Date;
  constructor(private musicService:MusicService) {
    this.track = new Track();
    musicService.currentTrack.subscribe((track:Track)=>{
      this.track = track;
      this.timeLeft = track.getTimeLeft();
      this.percentageIn = 100;
      this.timeIn = new Date(track.secondsIn*1000);
      if(this.timeLeft!=null){
        this.interval = setInterval(() => {
          this.timeLeft = track.getTimeLeft();
          this.percentageIn = track.percentagePlayed;
          this.timeIn = track.timeIn;
          if(this.timeLeft==null){
            clearInterval(this.interval);
          }
        }, 1000);
      }  
    });
  }
}