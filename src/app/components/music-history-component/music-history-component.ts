import {Component} from 'angular2/angular2';
import {Track} from '../../models/Track';
import {MusicService} from '../../services/music-service/music-service';

@Component({
  selector: 'music-history-component',
  templateUrl: 'app/components/music-history-component/music-history-component.html',
  styleUrls: ['app/components/music-history-component/music-history-component.css'],
  providers: [],
  directives: [],
  pipes: []
})
export class MusicHistoryComponent {
  tracks: Track[];
  constructor(private musicService:MusicService) {
    musicService.trackHistory.subscribe(tracks => {
      this.tracks = tracks
    });
    musicService.getHistory();
  }
}