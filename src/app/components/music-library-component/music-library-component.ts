import {Component} from 'angular2/angular2';
import {Track} from '../../models/Track';
import {MusicService} from '../../services/music-service/music-service';
import {Router} from 'angular2/router'

@Component({
  selector: 'music-library-component',
  templateUrl: 'app/components/music-library-component/music-library-component.html',
  styleUrls: ['app/components/music-library-component/music-library-component.css'],
  providers: [],
  directives: [],
  pipes: []
})
export class MusicLibraryComponent {
  tracks: Track[];
  constructor(private musicService:MusicService, private router:Router) {
    musicService.library.subscribe(tracks => this.tracks = tracks);
    musicService.getTracks();
  }
  onClick(trackId){
    this.musicService.addTrack(trackId);
    var song = '';
    for(var x=0; x<this.tracks.length; x++){
      if(this.tracks[x].trackId === trackId){
        song = this.tracks[x].title;
        break;
      }
    }
    this.router.navigate(['QueueAddedSong',{song:song}]);
  }
}