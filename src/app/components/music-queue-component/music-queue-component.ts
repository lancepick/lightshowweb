import {Component} from 'angular2/angular2';
import {Track} from '../../models/Track';
import {MusicService} from '../../services/music-service/music-service';
import {Router, RouteParams, RouterLink, Location} from 'angular2/router';

@Component({
  selector: 'music-queue-component',
  templateUrl: 'app/components/music-queue-component/music-queue-component.html',
  styleUrls: ['app/components/music-queue-component/music-queue-component.css'],
  providers: [],
  directives: [],
  pipes: []
})
export class MusicQueueComponent {
  tracks : Track[];
  song : string;
  constructor(private musicService:MusicService, params:RouteParams) {
    var song = params.get('song');
    if(!!song)
      this.song = song;
    musicService.trackQueue.subscribe(tracks => this.tracks=tracks);
    musicService.getQueue();
  }
}