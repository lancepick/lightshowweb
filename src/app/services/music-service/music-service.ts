import {Injectable, bind, Observable} from 'angular2/angular2';
import {Http, Response} from 'angular2/http'
import {GlobalSettingsService} from '../global-settings-service/global-settings-service'
import {Track} from '../../models/track'

@Injectable()
export class MusicService {
  currentTrack: Observable<Track>;
  trackQueue: Observable<Array<Track>>;
  trackHistory: Observable<Array<Track>>;
  library: Observable<Array<Track>>;
  
  private _dataStore: {
    currentTrack: Track,
    trackQueue: Array<Track>,
    trackHistory: Array<Track>,
    library: Array<Track>
  };
  
  private _observers: {
    currentTrack: any,
    trackQueue: any,
    trackHistory: any,
    library: any
  };
  
  private _baseUrl: string;
  private _nowPlayingTimeout: number;
  private _queueTimeout: number;
  
  constructor(private globalSettings: GlobalSettingsService,private _http: Http) {
    this._baseUrl = globalSettings.baseUrl;
    this._observers = {
      currentTrack: null,
      trackQueue: null,
      trackHistory: null,
      library: null
    };

    this.currentTrack = new Observable(observer=>this._observers.currentTrack = observer).share();
    this.trackQueue = new Observable(observer=>this._observers.trackQueue = observer).share();
    this.trackHistory = new Observable(observer=>this._observers.trackHistory = observer).share();
    this.library = new Observable(observer=>this._observers.library = observer).share();
    
    this._dataStore = {
      currentTrack : null,
      trackQueue: [],
      trackHistory: [],
      library: []
    };
      
    this.getNowPlaying();
  }
  
  getTracks(){
    var path = this._baseUrl + '/api/Tracks';
    this._http.get(path).map((res: Response) => res.json()).subscribe((data:Track[])=>{
      this._dataStore.library = data;
      this._observers.library.next(this._dataStore.library);
    }, error=>console.log('error loading Library: ', error));
  }
  
  addTrack(trackId: number){
    var path = this._baseUrl + '/api/Playlist/Add/'+trackId;
    this._http.get(path).map((res: Response) => res.json()).subscribe(data=>{
      this.getQueue();
    }, error => console.log('error adding Track: ', error));
  }
  
  getQueue(){
    var path = this._baseUrl + '/api/Queue';
    this._http.get(path).map((res: Response) => res.json()).subscribe((data:Track[]) => {
      this._dataStore.trackQueue = data;
      if(!!this._observers.trackQueue)
        this._observers.trackQueue.next(this._dataStore.trackQueue);
      if(this._queueTimeout!=null){
        clearTimeout(this._queueTimeout);
      }
      this._queueTimeout = setTimeout(()=>this.getQueue(), (30*1000))
    }, error => console.log('error loading Queue: ', error));
  }
  
  getHistory(){
    var path = this._baseUrl + '/api/Playlist/History';
    this._http.get(path).map((res: Response) => res.json()).subscribe((data:Track[]) => {
      this._dataStore.trackHistory = data.map((t)=>new Track(t));
      if(!!this._observers.trackHistory)
        this._observers.trackHistory.next(this._dataStore.trackHistory);
    }, error => console.log('error loading History: ', error));
  }
  
  getNowPlaying(){
    var path = this._baseUrl + '/api/NowPlaying';
    var timeLeftMillis = (30*1000); // default 30 seconds
    this._http.get(path).map((res: Response) => res.json()).subscribe((data:Track) => {
      if(!!data){
        this._dataStore.currentTrack = new Track(data);
        var timeLeft = this._dataStore.currentTrack.getTimeLeft()
        if(timeLeft!=null){
          timeLeftMillis = timeLeft.getTime();
        }
        this._observers.currentTrack.next(this._dataStore.currentTrack);
      }
      this._nowPlayingTimeout = setTimeout(() => this.getNowPlaying(), timeLeftMillis);
    }, error => console.log('error loading Now Playing: ', error));
    this.getQueue();
    this.getHistory();
  }
}